import numpy as np
from matplotlib import pyplot as plt

def db(u1, u2):
	return 20*np.log10(u2/u1)
	print(u2/u1)

data37_10=np.loadtxt("sirka37-10.txt")
data37_100=np.loadtxt("sirka37-100.txt")
data741_100=np.loadtxt("sirka741-100.txt")
data741_10=np.loadtxt("sirka741-10.txt")



zes37_10=db(data37_10[:, 1], data37_10[:,2])
zes37_100=db(data37_100[:, 1], data37_100[:,2])


zes741_10=db(data741_10[:, 1], data741_10[:,2])
zes741_100=db(data741_100[:, 1], data741_100[:,2])


plt.plot(data37_10[:,0], zes37_10, label="OP37 10x")
plt.plot(data37_100[:,0], zes37_100, label="OP37 100x")

plt.plot(data741_10[:,0], zes741_10, label="741 10x")
plt.plot(data741_100[:,0], zes741_100, label="741 100x")
plt.semilogx()
plt.legend()
plt.xlabel("f [Hz]")
plt.ylabel("A [dB]")
plt.show()
