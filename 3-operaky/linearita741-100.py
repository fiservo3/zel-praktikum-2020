import numpy as np
from matplotlib import pyplot as plt

data=np.loadtxt("linearita741-100.txt")

U_in=data[:, 0]
U_out=data[:, 1]

plt.plot(U_in, U_out, ".")
plt.show()
