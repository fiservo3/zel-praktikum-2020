import numpy as np
from matplotlib import pyplot as plt

data741=np.loadtxt("linearita741-100.txt")
data37=np.loadtxt("linearita37-100.txt")

fig, ax = plt.subplots()

ax.plot(data741[:,0], data741[:,1], "x", label = "uA741")
ax.plot(data37[:,0], data37[:,1], "+", label = "uA741")

ax.axhline(y=0, color='k', lw=0.5)
ax.axvline(x=0, color='k', lw=0.5)
ax.set_xlabel("Uin [V]")
ax.set_ylabel("Uout [V]")
plt.legend()
plt.show()
