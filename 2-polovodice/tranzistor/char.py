#import numpy as np
from matplotlib import pyplot as plt
import numpy as np
from scipy import optimize

Rc=1.2e3
Rb=47e3

data1 = np.loadtxt("char_data1.txt")
URb1 = .169

data2 = np.loadtxt("char_data2.txt")
URb2 = 0.333

data3 = np.loadtxt("char_data3.txt")
URb3 = 0.856


Uce1 = data1[:,0]
Ic1 = data1[:,1]/Rc
Ib1 = URb1/Rb

Uce2 = data2[:,0]
Ic2 = data2[:,1]/Rc

Ib2= URb2/Rb

Uce3 = data3[:,0]
Ic3 = data3[:,1]/Rc
Ib3= URb3/Rb

plt.plot(Uce1, 1e3*Ic1, label="Ib = 3,6 uA")
plt.plot(Uce2, 1e3*Ic2, label="Ib = 7,1 uA")
plt.plot(Uce3, 1e3*Ic3, label="Ib = 18,2 uA")
plt.xlabel("Uce [V]")
plt.ylabel("Ic [mA]")

plt.legend()
plt.show()

beta1=Ic1/Ib1
beta2=Ic2/Ib2
beta3=Ic3/Ib3

print(beta1)
print(beta2)
print(beta3)

plt.plot(Uce1, beta1, label="Ib = "+str(Ib1))
plt.plot(Uce2, beta2, label="Ib = "+str(Ib2))
plt.plot(Uce2, beta3, label="Ib = "+str(Ib3))
plt.xlabel("Uce")
plt.ylabel("beta")
plt.show()