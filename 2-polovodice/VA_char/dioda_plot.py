import numpy as np
import math

from matplotlib import scale as mscale
from matplotlib import transforms as mtransforms
from matplotlib.ticker import FuncFormatter

def conditionalScale(x, a, threshold=0):
	result=np.zeros_like(x)
	for i in range(x.shape[0]):
		if(x[i]>=threshold):
			result[i] = x[i]
		else:
			result[i] = x[i]*a
	return result


#Toto mi nedavalo dobre vysledky
#kdybyste chteli, tak si muzete hrat
'''
def conditionalScale_semilog(x, a):
	result=np.zeros_like(x)
	for i in range(x.shape[0]):
		if(x[i]>=0):
			result[i] = x[i]
		else:
			result[i] = -math.log(-x[i])*a
	return result

def conditionalScale_semilog_i(x, a):
	result=np.zeros_like(x)
	for i in range(x.shape[0]):
		if(x[i]>=0):
			result[i] = x[i]
		else:
			result[i] = -math.pow(10,(-x[i]))*a
	return result
'''

x_scale_coeff = 1/25
y_scale_coeff = 1000

def scale_x(x):
	return conditionalScale(x, x_scale_coeff)
def scale_x_inv(x):
	return conditionalScale(x, 1/x_scale_coeff)


def scale_y(x):
	return conditionalScale(x, y_scale_coeff)
def scale_y_inv(x):
	return conditionalScale(x, 1/y_scale_coeff)
'''
zkouska=np.array([0, 1, 2, -1, -2, 0, -14, -15, 30], dtype="float64")
print(zkouska)
print(scale_x(zkouska))
print(scale_x_inv(zkouska))
print(scale_x_inv(scale_x (zkouska)))
'''

import matplotlib.pyplot as plt
fig, ax = plt.subplots()


data_ge_prop=np.loadtxt("dioda-hrotova-prop.txt")
data_ge_zav=-(np.loadtxt("dioda-hrot-zav.txt"))
data_ge_hor=-(np.loadtxt("Germanium_horka.txt"))


data_si_prop=np.loadtxt("dioda-1n4007-propustny.txt")
data_si_zav=-(np.loadtxt("dioda-1n4007-zaver.txt"))


data_ge_prop=np.loadtxt("dioda-hrotova-prop.txt")
data_ge_zav=-(np.loadtxt("dioda-hrot-zav.txt"))

data_sch_prop=np.loadtxt("dioda-schottky-propustny.txt")
data_sch_zav=-(np.loadtxt("dioda-schottky-zaver.txt"))


data_ze_prop=np.loadtxt("dioda-BZX85V008_2-prop.txt")
data_ze_zav=-(np.loadtxt("dioda-BZX85V008_2-zaver.txt"))

ax.set_xlim(-26, 0.8)
ax.set_ylim(-6.5e-6, 1.1e-3)

ax.plot(*data_ge_prop.T, label="Germaniová", color="orange")
ax.plot(*data_ge_zav.T, color="orange")
ax.plot(*data_ge_hor.T, label = "Germaniová ohřátá", linestyle="--",color="orange")

ax.plot(*data_si_prop.T, label="Křemíková", color="green")
ax.plot(*data_si_zav.T, color="green")

ax.plot(*data_sch_prop.T, label="Schottky", color="red")
ax.plot(*data_sch_zav.T, color="red")

ax.plot(*data_ze_prop.T, label="Zenerova", color="blue")
ax.plot(*data_ze_zav.T, color="blue")


ax.axhline(y=0, color='k', lw=0.5)
ax.axvline(x=0, color='k', lw=0.5)

ax.set_xscale('function', functions=(scale_x,scale_x_inv))
ax.set_yscale('function', functions=(scale_y,scale_y_inv))
ax.set_xticks([-25, -20, -15, -10, -5, 0, 0.2, 0.4, 0.6, 0.8, 1])
ax.set_yticks([-6e-6, -4e-6, -2e-6,0, 2e-3, 4e-3, 6e-3, 8e-3, 10e-3])
ax.set_yticklabels(["-600 uA", "-400 uA", "-200 uA", "0", "2mA", "4mA","6mA", "8mA", "10mA", ])

plt.xlabel("U [V]")
plt.ylabel("I")
plt.legend()
plt.show()